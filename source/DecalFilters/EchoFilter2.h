// EchoFilter.h : Declaration of the cEchoFilter

#ifndef __ECHOFILTER2_H_
#define __ECHOFILTER2_H_

#include "resource.h"       // main symbols

#include <DecalNetImpl.h>
#include "DecalFiltersCP.h"

/////////////////////////////////////////////////////////////////////////////
// cEchoFilter
class ATL_NO_VTABLE cEchoFilter2 : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cEchoFilter2, &CLSID_EchoFilter2>,
	public ISoloNetworkFilterImpl< cEchoFilter2 >,
   public IProvideClassInfo2Impl< &CLSID_EchoFilter2, &DIID_IEchoSink2, &LIBID_DecalFilters >,
	public IConnectionPointContainerImpl<cEchoFilter2>,
   public IDispatchImpl<IEcho2, &IID_IEcho2, &LIBID_DecalFilters >,
   public CProxyIEchoSink2< cEchoFilter2 >
{
public:
	cEchoFilter2()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ECHOFILTER2)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cEchoFilter2)
   COM_INTERFACE_ENTRY(IEcho2)
   COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(INetworkFilter2)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cEchoFilter2)
CONNECTION_POINT_ENTRY(DIID_IEchoSink2)
END_CONNECTION_POINT_MAP()

// IEchoFilter2
public:
	STDMETHOD(DispatchServer)(IMessage2 *);
	STDMETHOD(DispatchClient)(IMessage2 *);
};

#endif //__ECHOFILTER2_H_
