// EchoFilter.h : Declaration of the cEchoFilter

#ifndef __ECHOFILTER_H_
#define __ECHOFILTER_H_

#include "resource.h"       // main symbols

#include "DecalFiltersCP.h"

/////////////////////////////////////////////////////////////////////////////
// cEchoFilter
class ATL_NO_VTABLE cEchoFilter : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cEchoFilter, &CLSID_EchoFilter>,
	public IDispatchImpl< IEcho, &IID_IEcho, &LIBID_DecalFilters >,
   public IDispatchImpl< INetworkFilter, &IID_INetworkFilter, &LIBID_DecalNet >,
   public IProvideClassInfo2Impl< &CLSID_EchoFilter, &DIID_IEchoSink, &LIBID_DecalFilters >,
	public IConnectionPointContainerImpl<cEchoFilter>,
	public CProxyIEchoSink< cEchoFilter >
{
public:
	cEchoFilter()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ECHOFILTER)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cEchoFilter)
	COM_INTERFACE_ENTRY(INetworkFilter)
   COM_INTERFACE_ENTRY(IEcho)
   COM_INTERFACE_ENTRY2(IDispatch,INetworkFilter)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cEchoFilter)
CONNECTION_POINT_ENTRY(DIID_IEchoSink)
END_CONNECTION_POINT_MAP()

// IEchoFilter
public:
	STDMETHOD(Dispatch)(IMessage *);
};

#endif //__ECHOFILTER_H_
