// WorldObject.h : Declaration of the cWorldObject

#ifndef __WORLDOBJECT_H_
#define __WORLDOBJECT_H_

#include "resource.h"       // main symbols

#define FLAG_STACKABLE			0x00000001
#define FLAG_LOCATION			0x00000002
#define FLAG_LOCKABLE			0x00000004
#define FLAG_INSCRIBABLE		0x00000010
#define FLAG_USEABLE			0x00000020
#define FLAG_HASSPELL			0x00000040
#define FLAG_CONTAINS			0x00000080

class cWorld;
class cWorldData;

/////////////////////////////////////////////////////////////////////////////
// cWorldObject
class ATL_NO_VTABLE cWorldObject : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cWorldObject, &CLSID_WorldObject>,
	public IDispatchImpl<IWorldObject, &IID_IWorldObject, &LIBID_DecalFilters>,
	public IWorldObject2
{
public:
	cWorldObject()
	{
	}
	virtual ~cWorldObject();

DECLARE_REGISTRY_RESOURCEID(IDR_WORLDOBJECT)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cWorldObject)
	COM_INTERFACE_ENTRY(IWorldObject)
	COM_INTERFACE_ENTRY2(IDispatch, IWorldObject)
	COM_INTERFACE_ENTRY(IWorldObject2)
END_COM_MAP()

private:
	HRESULT GetLong(long val, long *ret) ;
public:
	// IWorldObject
	STDMETHOD(get_Coordinates)(/*[in, out]*/ float *NorthSouth, /*[in, out]*/ float *EastWest, /*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_RawCoordinates)(/*[in, out]*/ float *pX, /*[in, out]*/ float *pY, /*[in, out]*/ float *pZ, /*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_Distance)(float *NorthSouth, float *EastWest, /*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_WieldingSlot)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Wielder)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Owner)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Slot)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_AssociatedSpell)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_StackMax)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_StackCount)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TotalUses)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_UsesLeft)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Inscribable)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_Lockable)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_PackSlots)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_ItemSlots)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Type)(/*[out, retval]*/ eObjectType *pVal);
	STDMETHOD(get_Heading)(float *x, float *y, float *z, float *w, /*[out, retval]*/ short *pVal);
	STDMETHOD(get_Offset)(float *x, float *y, float *z, /*[out, retval]*/ short *pVal);
	STDMETHOD(get_Landblock)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Container)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Value)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Name)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Icon)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Model)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_GUID)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Monarch)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Material)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Coverage)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Coverage2)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Coverage3)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TradeNoteVendor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_EquipType)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_SecondaryName)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_ApproachDistance)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_Workmanship)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_Burden)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_IconOutline)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_MissileType)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TotalValue)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_UsageMask)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_HouseOwner)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_HookMask)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_HookType)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_RealModel)(/*[out, retval]*/long *pVal);
	STDMETHOD(get_Scale)(/*[out, retval]*/float *pVal);
	STDMETHOD(get_HasIdData)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_ArmorLevel)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_MagicDef)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Spellcraft)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_MaximumMana)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_LoreReq)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_RankReq)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_SkillReq)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_WieldReqType)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_WieldReqId)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_WieldReq)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TinkerCount)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_SkillReqId)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_SpecialProps)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_ManaCMod)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_SpellCount)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Spell)(long index, /*[out, retval]*/ long *pVal);
	STDMETHOD(get_Inscription)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_WeapSpeed)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_EquipSkill)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_DamageType)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_MaxDamage)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Variance)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_DefenseBonus)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_AttackBonus)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_Range)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_SlashProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_PierceProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_BludProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_ColdProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_FireProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_ElectProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_AcidProt)(/*[out, retval]*/ float *pVal);
	STDMETHOD(get_RaceReq)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_DamageBonus)(/*[out, retval]*/ float *pVal);

	// IWorldObject2
	STDMETHOD(get_Flags)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_CreateFlags1)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_CreateFlags2)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_ObjectFlags1)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_ObjectFlags2)(/*[out, retval]*/ long *pVal);

	cWorldData *m_p;
};

#endif //__WORLDOBJECT_H_
