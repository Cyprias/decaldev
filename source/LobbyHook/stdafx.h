// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define _WIN32_WINDOWS 0x0410
// Windows Header Files:
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <atlbase.h>
