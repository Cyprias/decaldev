// PluginSite.cpp : Implementation of cPluginSite
#include "stdafx.h"
#include "Decal.h"
#include "PluginSite.h"

#include "DecalManager.h"

/////////////////////////////////////////////////////////////////////////////
// cPluginSite

void cPluginSite::FinalRelease()
{
   _ASSERTE( m_pDecal != NULL );

   for( cDecal::cPluginList::iterator i = m_pDecal->m_plugins.begin(); i != m_pDecal->m_plugins.end(); ++ i )
   {
      if( i->m_pSite == this )
      {
         m_pDecal->m_plugins.erase( i );
         return;
      }
   }

   // Could not find ourself in the list (bad initialization?)
   _ASSERT( FALSE );
}

STDMETHODIMP cPluginSite::Unload()
{
   // Hold a reference to ourself, to keep everything 'together' until
   // the method returns
   CComPtr< IUnknown > pThis( this );

   m_pPlugin->Terminate();

   m_pPlugin.Release();

	return S_OK;
}

STDMETHODIMP cPluginSite::get_Decal(IDecal **pVal)
{
	return m_pDecal->QueryInterface( IID_IDecal, reinterpret_cast< void ** >( pVal ) );
}

STDMETHODIMP cPluginSite::get_Object(BSTR Path, LPDISPATCH *pVal)
{
	return m_pDecal->get_Object( Path, IID_IDispatch, reinterpret_cast< void ** >( pVal ) );
}
