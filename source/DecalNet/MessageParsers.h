// MessageParsers.h
// Declaration of class cMessageParser - base class for parsing the
// XML Schema

#ifndef __MESSAGEPARSERS_H
#define __MESSAGEPARSERS_H

#include "Message.h"

class cElementParser
{
public:
   class cContext
   {
      cContext *m_pParent;
      cMessage::cElementList *m_pElements;
      DWORD m_dwStartOffset;

   public:
      cContext( cMessage::cElementList *pElements, cContext *pParent = NULL )
         : m_pParent( pParent ),
         m_pElements( pElements ),
         m_dwStartOffset( pElements->size() )
      {
      }

      cMessage::cMessageElement *findElement( const _bstr_t &strElement );
      bool parseChildren( MSXML::IXMLDOMElementPtr &pElement );
      bool parseChildren( cMessage::cElementList &elements, MSXML::IXMLDOMElementPtr &pElement );

      static _bstr_t g_strAll;
   };

   virtual cMessage::cMessageElement *parse( cContext &context, MSXML::IXMLDOMElementPtr &pElement ) = 0;

   static cElementParser *lookup( const _bstr_t &strElement );

   static void init();
   static void term();

private:
   static void addParser( LPCTSTR szElement, cElementParser *pParser );

   typedef std::map< _bstr_t, VSBridge::auto_ptr< cElementParser > > cElementParserMap;
   static cElementParserMap g_parsers;
};

#endif
