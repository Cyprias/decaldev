// FilterAdapterV1.h
// Declaration of the filter adapter for V1 filters

#ifndef __FILTERADAPTER_H
#define __FILTERADAPTER_H

#include <DecalNetImpl.h>

class cFilterAdapterV1
: public CComObjectRootEx< CComMultiThreadModel >,
public ISoloNetworkFilterImpl< cFilterAdapterV1 >
{
   CComPtr<IUnknown> m_pV1;

public:
   HRESULT init ( REFCLSID clsid );

BEGIN_COM_MAP(cFilterAdapterV1)
   COM_INTERFACE_ENTRY(INetworkFilter2)
   COM_INTERFACE_ENTRY_AGGREGATE_BLIND(m_pV1.p)
END_COM_MAP()

public:
   // INetworkFilter2
   STDMETHOD(DispatchServer)(IMessage2 *pMessage);
};

#endif
