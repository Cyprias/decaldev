// FilterAdapterV1.cpp
// Implementation of class cFilterAdapterV1

#include "Stdafx.h"
#include "FilterAdapterV1.h"

HRESULT cFilterAdapterV1::init ( REFCLSID clsid )
{
   HRESULT hRes = ::CoCreateInstance ( clsid, NULL, CLSCTX_INPROC_SERVER, IID_IUnknown,
      reinterpret_cast< LPVOID * > ( &m_pV1 ) );
   _ASSERTE( SUCCEEDED ( hRes ) );
   return hRes;
}

STDMETHODIMP cFilterAdapterV1::DispatchServer ( IMessage2 *pMessage )
{
   CComPtr<INetworkFilter> pNF;
   HRESULT hRes = m_pV1->QueryInterface ( &pNF );
   _ASSERTE( SUCCEEDED ( hRes ) );
   return pNF->Dispatch ( pMessage );
}
