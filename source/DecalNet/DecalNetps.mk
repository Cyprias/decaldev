
DecalNetps.dll: dlldata.obj DecalNet_p.obj DecalNet_i.obj
	link /dll /out:DecalNetps.dll /def:DecalNetps.def /entry:DllMain dlldata.obj DecalNet_p.obj DecalNet_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DecalNetps.dll
	@del DecalNetps.lib
	@del DecalNetps.exp
	@del dlldata.obj
	@del DecalNet_p.obj
	@del DecalNet_i.obj
