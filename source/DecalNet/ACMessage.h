#pragma once

class ACMessage
{
public:
	virtual BYTE *getData () = 0;
	virtual DWORD getSize () = 0;
	virtual DWORD getType () = 0;
};

class ACMessageSink
{
public:
	virtual void onMessage( ACMessage& ) = 0;
	virtual void onMessageOut( ACMessage& ) = 0;
};
