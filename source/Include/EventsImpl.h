// EventsImpl.h
// Declaration of helper class for implementing plugins

#ifndef _EVENTSIMPL_H_
#define _EVENTSIMPL_H_

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl, const IID *pIID, const GUID *pLIBID >
class IControlEventsImpl
: public IDispEventImpl< nID, cImpl, pIID, pLIBID, 1, 0 >
{
protected:
   void advise( IUnknown *pUnk )
   {
      HRESULT hRes = DispEventAdvise( pUnk );

      _ASSERTE( SUCCEEDED( hRes ) );
   }

   void unadvise( IUnknown *pUnk )
   {
      HRESULT hRes = DispEventUnadvise( pUnk );

      _ASSERTE( SUCCEEDED( hRes ) );
   }
};

#define DISPID_DESTROY 1


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class ICommandEventsImpl
: public IControlEventsImpl< nID, cImpl, &DIID_ICommandEvents, &LIBID_DecalPlugins > 
{
};

#define DISPID_HIT      2
#define DISPID_UNHIT    3
#define DISPID_ACCEPTED 4
#define DISPID_CANCELED 5


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IPagerEventsImpl
: public IControlEventsImpl< nID, cImpl, &DIID_IPagerEvents, &LIBID_DecalPlugins >
{
};

#define DISPID_CHANGE            6
#define DISPID_GETNEXTPOSITION   7
#define DISPID_UPDATE_POSITION   6
#define DISPID_GET_NEXT_POSITION 7


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IViewEventsImpl
: public IControlEventsImpl< nID, cImpl, &DIID_IViewEvents, &LIBID_DecalPlugins >
{
};

#define DISPID_ACTIVATE   8
#define DISPID_DEACTIVATE 9
#define DISPID_SIZE       13
#define DISPID_SIZING     14


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IInputEventsImpl
: public IControlEventsImpl< nID, cImpl, &DIID_IInputEvents, &LIBID_DecalPlugins >
{
};

#define DISPID_BEGIN 10
#define DISPID_END   11
#define DISPID_PAUSE 12

#endif // _EVENTSIMPL_H_
