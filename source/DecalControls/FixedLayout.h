// Client.h : Declaration of the cClient

#ifndef __FIXEDLAYOUT_H_
#define __FIXEDLAYOUT_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "ControlImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cClient
class ATL_NO_VTABLE cFixedLayout : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cFixedLayout, &CLSID_FixedLayout>,
   public ILayerImpl< cFixedLayout >,
   public ILayoutImpl< cFixedLayout, IFixedLayout, &IID_ILayout, &LIBID_DecalControls >,
   public IProvideClassInfo2Impl< &CLSID_FixedLayout, &DIID_IControlEvents, &LIBID_DecalControls >,
   public IConnectionPointContainerImpl<cFixedLayout>,
   public CProxyIControlEvents< cFixedLayout >
{
public:
	cFixedLayout()
	{
	}

   void onSchemaLoad( IView *pView, MSXML::IXMLDOMElementPtr &pElement );

DECLARE_REGISTRY_RESOURCEID(IDR_FIXEDLAYOUT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cFixedLayout)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(ILayout)
	COM_INTERFACE_ENTRY(IFixedLayout)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cFixedLayout)
CONNECTION_POINT_ENTRY(DIID_IControlEvents)
END_CONNECTION_POINT_MAP()

public:
   // IFixedLayout Methods
	STDMETHOD(CreateChild)(struct LayerParams *params, ILayer *pSink);
	STDMETHOD(PositionChild)(long nID, LPRECT prcNew);
};

#endif //__CLIENT_H_
