// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__2E05D2E0_3EAC_488F_8C1B_3CF7378ADE73__INCLUDED_)
#define AFX_STDAFX_H__2E05D2E0_3EAC_488F_8C1B_3CF7378ADE73__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#define _WIN32_WINDOWS 0x0410
#define _ATL_APARTMENT_THREADED

// Don't use macros for min() and max()!!!
#define NOMINMAX

#include <atlbase.h>
#include <list>
#include <vector>
#include <algorithm>
#include <deque>
#include <map>
#include <string>

// Keep things backward compatible
#if defined(_MSC_VER) && _MSC_VER <= 1200
#define min std::_cpp_min
#define max std::_cpp_max
#else
using std::min;
using std::max;
#endif

//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

#include "..\Inject\Inject.h"
#import <msxml.dll>

#ifdef _CHECKMEM
#define _ASSERTMEM(a) _ASSERTE(a)
#else
#define _ASSERTMEM(a)
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__2E05D2E0_3EAC_488F_8C1B_3CF7378ADE73__INCLUDED)
