// Checkbox.h : Declaration of the cCheckbox

#ifndef __SLIDER_H_
#define __SLIDER_H_

#include "resource.h"       // main symbols
#include "DecalControlsCP.h"

#include "SinkImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cSlider
class ATL_NO_VTABLE cSlider : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cSlider, &CLSID_Slider>,
	public IControlImpl< cSlider, ISlider, &IID_ISlider, &LIBID_DecalControls >,
	public ILayerMouseImpl,
	public ILayerRenderImpl,
	public ILayerImpl< cSlider >,
	public ILayerSchema,
	public IProvideClassInfo2Impl< &CLSID_Slider, &DIID_ISliderEvents, &LIBID_DecalControls >,
	public IConnectionPointContainerImpl<cSlider>,
	public CProxyISliderEvents<cSlider>
{
public:
	cSlider();
	~cSlider();

	CComPtr<IFontCache> m_pFont;

	VARIANT_BOOL m_bVertical;
	VARIANT_BOOL m_bMouseIn;
	VARIANT_BOOL m_bGrappled;

	long m_nTextColor;
	float m_nSliderPos;
	long m_nMinimum, m_nMaximum;
	long m_nMinSliderColor, m_nMaxSliderColor;

	char *m_strTextLeft, *m_strTextRight;

	POINT m_ptSlider, m_ptLastMouse;
	POINT m_ptLeftText, m_ptRightText;
	RECT m_rcBar, m_rcBackground;

DECLARE_REGISTRY_RESOURCEID(IDR_SLIDER)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cSlider)
	COM_INTERFACE_ENTRY(ILayerRender)
	COM_INTERFACE_ENTRY(ILayerMouse)
	COM_INTERFACE_ENTRY(ILayerSchema)
	COM_INTERFACE_ENTRY(ISlider)
	COM_INTERFACE_ENTRY(IControl)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cSlider)
CONNECTION_POINT_ENTRY(DIID_ISliderEvents)
END_CONNECTION_POINT_MAP()

public:
	// ISlider Methods
	STDMETHOD(get_TextColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);

	STDMETHOD(get_Font)(/*[out, retval]*/ IFontCacheDisp * *pVal);
	STDMETHOD(putref_Font)(/*[in]*/ IFontCacheDisp * newVal);

	STDMETHOD(get_SliderPosition)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_SliderPosition)(/*[in]*/ long newVal);

	STDMETHOD(get_Minimum)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Minimum)(/*[in]*/ long newVal);

	STDMETHOD(get_Maximum)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Maximum)(/*[in]*/ long newVal);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pSchema);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
	STDMETHOD(MouseDown)(struct MouseState *mouseState);
	STDMETHOD(MouseUp)(struct MouseState *mouseState);
	STDMETHOD(MouseMove)(struct MouseState *mouseState);
};
#endif //__SLIDER_H_
