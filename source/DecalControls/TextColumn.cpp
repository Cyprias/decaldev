// TextColumn.cpp : Implementation of cTextColumn
#include "stdafx.h"
#include "DecalControls.h"
#include "TextColumn.h"

/////////////////////////////////////////////////////////////////////////////
// cTextColumn

STDMETHODIMP cTextColumn::get_FixedWidth(VARIANT_BOOL *pVal)
{
   if( m_nFixedWidth == -1 )
      *pVal = VARIANT_FALSE;
   else
      *pVal = VARIANT_TRUE;

	return S_OK;
}

STDMETHODIMP cTextColumn::get_Width(long *pVal)
{
   if( m_nFixedWidth == -1 )
	   return E_NOTIMPL;

   *pVal = m_nFixedWidth;

   return S_OK;
}

STDMETHODIMP cTextColumn::put_Width(long newVal)
{
   if( m_nFixedWidth == -1 )
	   return E_NOTIMPL;

   m_nFixedWidth = newVal;

   return S_OK;
}

STDMETHODIMP cTextColumn::Render(ICanvas *pCanvas, LPPOINT ptCell, long nColor)
{
	USES_CONVERSION;

   _variant_t vText;
   m_pList->get_Data( ptCell->x, ptCell->y, 0, &vText );

   vText.ChangeType( VT_BSTR );

   CComPtr< IFontCache > pFont;
   BSTR bstrFontName;
   m_pSite->get_FontName(&bstrFontName);
   m_pSite->CreateFont( bstrFontName /*_bstr_t( _T( "Times New Roman" ) )*/, m_nFontSize, 0, &pFont );

   POINT pt = { m_nTextX, m_nTextY };
	
	// GKusnick: Apply justification.
	switch (m_eJustify)
	{
	case eFontRight:
		{
			SIZE Size;
			pFont->MeasureText(vText.bstrVal, &Size);
			pt.x = m_nFixedWidth - m_nTextX - Size.cx;
		}
		break;

	case eFontCenter:
		{
			SIZE Size;
			pFont->MeasureText(vText.bstrVal, &Size);
			pt.x = (m_nFixedWidth - Size.cx)/2;
		}
		break;

	case eFontLeft:
	default:
		{
			// No-Op
		}
		break;
	}
   
   pFont->DrawTextEx( &pt, vText.bstrVal, nColor, 0, eAA, pCanvas );

	return S_OK;
}

STDMETHODIMP cTextColumn::get_DataColumns(long *pVal)
{
   *pVal = 1;

	return S_OK;
}

STDMETHODIMP cTextColumn::Initialize(IList *newVal, IPluginSite *pSite)
{
   m_pList = newVal;
   m_pSite = pSite;

   // TODO: Make sure there is a row

	return S_OK;
}

STDMETHODIMP cTextColumn::SchemaLoad(IUnknown *pSchema)
{
   MSXML::IXMLDOMElementPtr pElement = pSchema;

   _variant_t vFixedWidth = pElement->getAttribute( _T( "fixedwidth" ) );
   _variant_t vRowHeight = pElement->getAttribute( _T( "rowheight" ) );
   _variant_t vFontSize = pElement->getAttribute( _T( "fontsize" ) );
   _variant_t vTextX = pElement->getAttribute( _T( "textx" ) );
   _variant_t vTextY = pElement->getAttribute( _T( "texty" ) );
   _variant_t vJustify = pElement->getAttribute( _T( "justify" ) );		/// GKusnick: Justification option.

   if( vFixedWidth.vt != VT_NULL )
   {
      try
      {
         m_nFixedWidth = static_cast< long >( vFixedWidth );
      }
      catch( ... )
      {
         // Type conversion error
         _ASSERTE( FALSE );
      }
   }

   if( vRowHeight.vt != VT_NULL )
   {
      try
      {
         m_nRowHeight = static_cast< long >( vRowHeight );
      }
      catch( ... )
      {
         // Type conversion error
         _ASSERTE( FALSE );
      }
   }
   else
	   m_nRowHeight = 20;


   if( vFontSize.vt != VT_NULL )
   {
      try
      {
         m_nFontSize = static_cast< long >( vFontSize );
      }
      catch( ... )
      {
         // Type conversion error
         _ASSERTE( FALSE );
      }
   }
   else
	   m_nFontSize = 14;

   if( vTextX.vt != VT_NULL )
   {
      try
      {
         m_nTextX = static_cast< long >( vTextX );
      }
      catch( ... )
      {
         // Type conversion error
         _ASSERTE( FALSE );
      }
   }
   else
	   m_nTextX = 3;

   if( vTextY.vt != VT_NULL )
   {
      try
      {
         m_nTextY = static_cast< long >( vTextY );
      }
      catch( ... )
      {
         // Type conversion error
         _ASSERTE( FALSE );
      }
   }
   else
	   m_nTextY = 2;
   
   // GKusnick: Set justification style from schema.
   m_eJustify = eFontLeft;
   if (vJustify.vt != VT_NULL)
   {
	   if (::_wcsicmp(L"left", vJustify.bstrVal) == 0)
	   {
		   m_eJustify = eFontLeft;
	   }
	   else if (::_wcsicmp(L"right", vJustify.bstrVal) == 0)
	   {
		   m_eJustify = eFontRight;
	   }
	   else if (::_wcsicmp(L"center", vJustify.bstrVal) == 0)
	   {
		   m_eJustify = eFontCenter;
	   }
   }

	return S_OK;
}

STDMETHODIMP cTextColumn::get_Height(long *pVal)
{
   *pVal = m_nRowHeight;

	return S_OK;
}

STDMETHODIMP cTextColumn::Activate(LPPOINT ptCell)
{
	// TODO: Add your implementation code here

	return S_OK;
}
