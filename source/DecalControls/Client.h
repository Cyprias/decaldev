// Client.h : Declaration of the cClient

#ifndef __CLIENT_H_
#define __CLIENT_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cClient
class ATL_NO_VTABLE cClient : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cClient, &CLSID_Client>,
   public ILayerImpl,
   public ILayerSchema,
	public IClient
{
public:
	cClient()
	{
	}

   CComPtr< ILayerSite > m_pSite;

   // TODO: Add support for background image and possibly
   // scrolling

DECLARE_REGISTRY_RESOURCEID(IDR_CLIENT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cClient)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IClient)
END_COM_MAP()

public:
   // IClient Methods

   // ILayer Methods
   STDMETHOD(LayerCreate)(ILayerSite *pSite);
   STDMETHOD(LayerDestroy)();

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pXMLSchema);
};

#endif //__CLIENT_H_
