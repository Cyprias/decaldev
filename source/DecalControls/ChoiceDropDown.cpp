// ChoiceDropDown.cpp : Implementation of cChoiceDropDown
#include "stdafx.h"
#include "DecalControls.h"
#include "ChoiceDropDown.h"

#include "Choice.h"

/////////////////////////////////////////////////////////////////////////////
// cChoiceDropDown

STDMETHODIMP cChoiceDropDown::Reformat()
{
   RECT rcLayer;
   m_pSite->get_Position( &rcLayer );

   // Move our only child to cover the entire client
   CComPtr< ILayerSite > pChild;
   m_pSite->get_Child( 0, ePositionByIndex, &pChild );

   if( ( rcLayer.right - rcLayer.left ) < 16 || ( rcLayer.bottom - rcLayer.top ) < 16 )
   {
      RECT rcClient = { 0, 0, 0, 0 };
      pChild->put_Position( &rcClient );
   }
   else
   {
      RECT rcClient = { 0, 0, rcLayer.right - rcLayer.left, rcLayer.bottom - rcLayer.top };
      pChild->put_Position( &rcClient );
   }

   return S_OK;
}

STDMETHODIMP cChoiceDropDown::PopupCancel(MouseState *pMS, VARIANT_BOOL *pbContinue)
{
   // Allow clicks in the parent object
   *pbContinue = ( pMS->over == m_pChoice ) ? VARIANT_TRUE : VARIANT_FALSE;

   if( !( *pbContinue ) )
   {
      m_pChoice->m_nHotSelect = -1;
      m_pChoice->setPopup( false );
   }

   return S_OK;
}