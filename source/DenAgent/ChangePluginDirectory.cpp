// cChangePluginDirectory.cpp : implementation file
//

#include "stdafx.h"
#include "DenAgent.h"
#include "ChangePluginDirectory.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// cChangePluginDirectory dialog


cChangePluginDirectory::cChangePluginDirectory(CWnd* pParent /*=NULL*/)
	: CDialog(cChangePluginDirectory::IDD, pParent)
{
	//{{AFX_DATA_INIT(cChangePluginDirectory)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void cChangePluginDirectory::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(cChangePluginDirectory)
	DDX_Control(pDX, IDC_NEWURL, m_NewUrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(cChangePluginDirectory, CDialog)
	//{{AFX_MSG_MAP(cChangePluginDirectory)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// cChangePluginDirectory message handlers

void cChangePluginDirectory::OnOK() 
{
	// TODO: Add extra validation here
	
   RegKey keyAgent;
   if( keyAgent.Open( HKEY_LOCAL_MACHINE, _T( "SOFTWARE\\Decal\\Agent" )) == ERROR_SUCCESS )
   {
	   CString csu;
	   m_NewUrl.GetWindowText(csu);
	   keyAgent.SetStringValue("DecalDirectory", csu);
	   keyAgent.Close();
   }
	CDialog::OnOK();
}

BOOL cChangePluginDirectory::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	RegKey keyAgent;
  if( keyAgent.Open( HKEY_LOCAL_MACHINE, _T( "SOFTWARE\\Decal\\Agent" )) != ERROR_SUCCESS )
  {
		::AfxMessageBox( _T( "Critical registry keys are missing, this installation is broken.\r\nRepair the installation." ), MB_ICONERROR | MB_OK, 0 );
		EndDialog( IDCANCEL );
		return FALSE;
  }

	TCHAR szUrl[ 1024 ];
  DWORD dwSize = 1024;

  keyAgent.QueryStringValue(_T("DecalDirectory"), szUrl, &dwSize);
  m_NewUrl.SetWindowText(szUrl);
  keyAgent.Close();
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
