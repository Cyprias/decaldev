#include "afxwin.h"
#if !defined(AFX_OPTIONSDLG_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_)
#define AFX_OPTIONSDLG_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// cOptionsDlg dialog

class cOptionsDlg : public CDialog
{
// Construction
public:
	cOptionsDlg(CWnd* pParent = NULL);   // standard constructor

	void UpdateCustomEdit(void);

	CString m_szDefaultFont;
	CString m_szACClientFont;

// Dialog Data
	//{{AFX_DATA(cOptionsDlg)
	enum { IDD = IDD_OPTIONS };
	CEdit	m_CustomFontEdit;
	CSpinButtonCtrl	m_ViewAlphaSpin;
	CSpinButtonCtrl	m_BarAlphaSpin;
	CEdit	m_BarAlpha;
	CEdit	m_ViewAlpha;
	CStatic	m_Url;
	int		m_nBarAlpha;
	int		m_nViewAlpha;
	int		m_nFontChoice;
	CString	m_szCustomFont;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cOptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool m_bClientPatchesEnabled;

	// Generated message map functions
	//{{AFX_MSG(cOptionsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeDir();
	afx_msg void OnReset();
	afx_msg void OnDefaultFontRadio();
	afx_msg void OnClientFontRadio();
	afx_msg void OnCustomFontRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOldInject();
	afx_msg void OnBnClickedCheckAutostart();
	afx_msg void OnBnClickedTimestampon();
	afx_msg void OnBnClickedFormathelp();
	afx_msg void OnBnClickedPatchHelp();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDLG_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_)
