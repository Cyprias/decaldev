#if !defined(AFX_DOWNLOADERDLG_H__EF5E9E0B_873E_4AFC_85D8_8F210D77AD79__INCLUDED_)
#define AFX_DOWNLOADERDLG_H__EF5E9E0B_873E_4AFC_85D8_8F210D77AD79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DownloaderDlg.h : header file
//

#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// cDownloaderDlg dialog

class cDownloaderDlg : public CDialog
{
// Construction
public:
	enum DownloadStatus
	{
		DOWNLOAD_SUCCEEDED,
		DOWNLOAD_FAILED
	};

	cDownloaderDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(cDownloaderDlg)
	enum { IDD = IDD_DOWNLOADER };
	CStatic	m_stStatusTotalData;
	CStatic	m_stStatusTotal;
	CButton	m_wndStop;
	CProgressCtrl	m_wndProgress;
	CProgressCtrl	m_wndProgressTotal;
	CStatic	m_stIEMsg;
	CStatic	m_stCustomMsg;
	//}}AFX_DATA

	CLSID m_clsid;
	CWinThread* m_pWorkerThread;

	bool m_bThreadDone;
	DownloadStatus GetDownloadStatus();
	
	unsigned long m_ulTotalData;

	void cDownloaderDlg::addFile(std::string remoteFile, std::string localFile);
	void ProgressUpdate ( LPCTSTR szIEMsg, LPCTSTR szCustomMsg, const int nPercentDone );
	void WorkerThreadProc();
	LRESULT OnEndDownload(WPARAM nID, LPARAM uMsg);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cDownloaderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(cDownloaderDlg)
	afx_msg void OnStopdownload();
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	std::vector<FILELIST> m_FileList;
	bool m_bDownloadSucceeded;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOWNLOADERDLG_H__EF5E9E0B_873E_4AFC_85D8_8F210D77AD79__INCLUDED_)
