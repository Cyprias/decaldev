// URLCallback.cpp : Implementation of cURLCallback
#include "stdafx.h"
#include "DenAgent.h"
#include "URLCallback.h"

#include "DownloadDlg.h"

/////////////////////////////////////////////////////////////////////////////
// cURLCallback

void cURLCallback::start( REFCLSID clsid, LPCWSTR szURL, DWORD dwMajor, DWORD dwMinor )
{
   // Create the bind context
   CComPtr< IBindCtx > pBindCtx;
   ::CreateAsyncBindCtx( 0, this, NULL, &pBindCtx );

   HRESULT hRes = ::CoCreateInstance( CLSID_InternetZoneManager, NULL, CLSCTX_SERVER, IID_IInternetZoneManager,
      reinterpret_cast< void ** >( &m_pZone ) );

   _ASSERTE( SUCCEEDED( hRes ) );

   m_pZone->GetZoneActionPolicy( URLZONE_INTERNET, URLACTION_DOWNLOAD_UNSIGNED_ACTIVEX, reinterpret_cast< BYTE * >( &m_dwOldPolicy ),
      sizeof( DWORD ), URLZONEREG_HKCU );

   DWORD dwPolicy = URLPOLICY_QUERY;
   m_pZone->SetZoneActionPolicy( URLZONE_INTERNET, URLACTION_DOWNLOAD_UNSIGNED_ACTIVEX, reinterpret_cast< BYTE * >( &dwPolicy ),
      sizeof( DWORD ), URLZONEREG_HKCU );

   hRes = ::CoGetClassObjectFromURL( clsid, szURL, 0xFFFFFFFF, 0xFFFFFFFF, L"application/x-oleobject",
      pBindCtx, CLSCTX_INPROC_SERVER, NULL, IID_IClassFactory, reinterpret_cast< void ** >( &m_pDlg->m_pFactory ) );

   if( FAILED( hRes ) )
   {
      m_pDlg->EndDialog( IDCANCEL );
      resetZone();
   }
   else if( hRes == S_OK )
   {
      m_pDlg->m_pFactory->LockServer( TRUE );
      m_pDlg->EndDialog( IDOK );
      resetZone();
   }

   // Otherwise, we'll assume that we're hapilly downloading asynchronously
}

void cURLCallback::stop()
{
   m_pDlg->m_wndStop.EnableWindow( FALSE );
   m_pBinding->Abort();
}

void cURLCallback::resetZone()
{
   m_pZone->SetZoneActionPolicy( URLZONE_INTERNET, URLACTION_DOWNLOAD_UNSIGNED_ACTIVEX, reinterpret_cast< BYTE * >( &m_dwOldPolicy ),
      sizeof( DWORD ), URLZONEREG_HKCU );
   m_pZone.Release();
}

STDMETHODIMP cURLCallback::GetBindInfo( DWORD *pgrfBINF, BINDINFO *pbindinfo )
{
   *pgrfBINF = BINDF_ASYNCHRONOUS | BINDF_GETNEWESTVERSION | BINDF_FROMURLMON | BINDF_IGNORESECURITYPROBLEM;
   
   // Not changing the defaults in pbindinfo
   return S_OK;
}

STDMETHODIMP cURLCallback::OnObjectAvailable( REFIID iid, IUnknown *pUnk )
{
   // Save the object pointer
   _ASSERTE( iid == IID_IClassFactory );

   m_pDlg->m_pFactory = static_cast< IClassFactory * >( pUnk );
   m_pDlg->m_pFactory->LockServer( TRUE );

   return S_OK;
}

STDMETHODIMP cURLCallback::OnProgress( ULONG ulProgress, ULONG ulProgressMax, ULONG, LPCWSTR szStatusText )
{
   USES_CONVERSION;

   // Update the status in the dialog
   m_pDlg->SetDlgItemText( IDC_STATUSTEXT, W2T( szStatusText ) );

   m_pDlg->m_wndProgress.SetRange32( 0, ulProgressMax );
   m_pDlg->m_wndProgress.SetPos( ulProgress );

   return S_OK;
}

STDMETHODIMP cURLCallback::OnStartBinding( DWORD, IBinding *pBinding )
{
   m_pBinding = pBinding;
   m_pDlg->m_wndStop.EnableWindow();

   return S_OK;
}

STDMETHODIMP cURLCallback::OnStopBinding( HRESULT hr, LPCWSTR strError )
{

   USES_CONVERSION;

   if( SUCCEEDED( hr ) )
      m_pDlg->EndDialog( IDOK );
   else
   {
      ::AfxMessageBox( W2T( strError ), MB_ICONERROR | MB_OK, 0 );
      m_pDlg->EndDialog( IDCANCEL );
   }

   resetZone();
   m_pBinding.Release();

   return S_OK;
}

STDMETHODIMP cURLCallback::GetWindow( REFGUID rguidReason, HWND *phwnd )
{
   *phwnd = m_pDlg->m_hWnd;
   return S_OK;
}
