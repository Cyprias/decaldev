// SinkImpl.cpp
// Implementation of empty functions

#include "stdafx.h"
#include "Inject.h"
#include "SinkImpl.h"

STDMETHODIMP ILayerRenderImpl::PreRender()
{
   return S_OK;
}

STDMETHODIMP ILayerRenderImpl::Render(ICanvas *)
{
   return S_OK;
}

STDMETHODIMP ILayerRenderImpl::Reformat()
{
	return S_OK;
}

STDMETHODIMP ILayerRenderImpl::AdjustRenderArea(ICanvas *, VARIANT_BOOL *pbDrawChildren)
{
   *pbDrawChildren = VARIANT_TRUE;
	return S_OK;
}

STDMETHODIMP ILayerRenderImpl::HitTest(LPPOINT pt, VARIANT_BOOL *pbHit)
{
   *pbHit = VARIANT_TRUE;

	return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseEnter(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseExit(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseDown(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseUp(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseMove(struct MouseState *)
{
   return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseDblClk(struct MouseState *)
{
   return S_OK;
}

STDMETHODIMP ILayerMouseImpl::MouseEvent(long nMsg, long wParam, long lParam)
{
   return S_OK;
}
