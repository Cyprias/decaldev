// Image.h : Declaration of the cImage

#ifndef __IMAGE_H_
#define __IMAGE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cImage
class ATL_NO_VTABLE cImage : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IDispatchImpl< IImageCache, &IID_IImageCacheDisp, &LIBID_DecalPlugins >
{
public:
	cImage()
	{
	}

   ~cImage();

   enum eSourceType
   {
      eFile,
      eResource,
      ePortal
   };

   eSourceType m_source;
   TCHAR m_szFilename[ MAX_PATH ];
   HMODULE m_hLibrary;
   DWORD m_dwID;

   CComPtr< ICanvas > m_pImage;

   void loadFile();
   void loadResource();
   void loadPortal();

   bool testImage();

BEGIN_COM_MAP(cImage)
	COM_INTERFACE_ENTRY(IImageCache)
   COM_INTERFACE_ENTRY(IImageCacheDisp)
   COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IImage
public:
	STDMETHOD(get_Size)(/*[out, retval]*/ SIZE *pVal);
	STDMETHOD(Blt)(LPRECT rcSrc, ICanvas *pDest, LPPOINT pptDest);
	STDMETHOD(StretchBlt)(ICanvas *pDest, LPPOINT pptDest, long nWidth, long nStartStretch, long nEndStretch);
	STDMETHOD(PatBlt)(ICanvas *pDest, LPRECT prcDest, LPPOINT ptOrigin);
	STDMETHOD(StretchBltArea)(LPRECT prcSrc, ICanvas *pDest, LPRECT prcDest);
};

#endif //__IMAGE_H_
