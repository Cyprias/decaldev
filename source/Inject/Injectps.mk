
Injectps.dll: dlldata.obj Inject_p.obj Inject_i.obj
	link /dll /out:Injectps.dll /def:Injectps.def /entry:DllMain dlldata.obj Inject_p.obj Inject_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del Injectps.dll
	@del Injectps.lib
	@del Injectps.exp
	@del dlldata.obj
	@del Inject_p.obj
	@del Inject_i.obj
