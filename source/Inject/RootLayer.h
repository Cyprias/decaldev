// RootLayer.h : Declaration of the cRootLayer

#ifndef __ROOTLAYER_H_
#define __ROOTLAYER_H_

#include "resource.h"       // main symbols

// This special layer implements no sinks, it is merely a container
// layer for the bar manager and the window manager - it server no other purpose
// than to forward event on and avoid redundant looping in the manager object.

#include "SinkImpl.h"

class cView;

/////////////////////////////////////////////////////////////////////////////
// cRootLayer
class ATL_NO_VTABLE cRootLayer : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public ILayerImpl< cRootLayer >,
	public IPanelSink,
	public cNoEventsImpl,
	public IRootLayer
{
public:
	cRootLayer() : m_nNextViewID( 1000 ), m_nViewMode( 0 )
	{
		RegKey key;
		key.Create( HKEY_LOCAL_MACHINE, _T( "SOFTWARE\\Decal" ) );
		if( key.QueryDWORDValue( _T("ViewMode"), m_nViewMode ) != ERROR_SUCCESS)
			m_nViewMode = 0;
	}

	DWORD m_nViewMode;

   CComPtr< IBarManager > m_pBars;
   CComPtr< IPanel > m_pPanel;

   typedef std::list< cView * > cViewList;
   cViewList m_views;
   long m_nNextViewID;
   long m_lDefaultX;
   long m_lDefaultY;

   void addView( cView *pView, ILayer *pRootLayer );
   void removeView( cView *pView );

   void onCreate();
   void onDestroy();

BEGIN_COM_MAP(cRootLayer)
   COM_INTERFACE_ENTRY(IPanelSink)
   COM_INTERFACE_ENTRY(IRootLayer)
   COM_INTERFACE_ENTRY(ILayer)
END_COM_MAP()

public:
   // IRootLayer Methods
	STDMETHOD(CreateView)(ViewParams *pParams, ILayer *pLayer, /*[out, retval]*/ IView **ppView);
	STDMETHOD(SelectBar)(long nID);
	STDMETHOD(LoadView)(BSTR strXML, /*[out, retval]*/ IView **ppView);
	STDMETHOD(LoadViewObject)(IUnknown *pSchema, /*[out, retval]*/ IView **ppView);
   // IPanelSink Methods
   STDMETHOD(PanelDeactivate)( long nViewID );
};

#endif //__ROOTLAYER_H_
