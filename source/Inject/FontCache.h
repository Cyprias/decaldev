// FontCache.h : Declaration of the cFontCache

#ifndef __FONTCACHE_H_
#define __FONTCACHE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cFontCache
class ATL_NO_VTABLE cFontCache : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IDispatchImpl< IFontCache, &IID_IFontCacheDisp, &LIBID_DecalPlugins >
{
public:
	cFontCache()
   {
		m_bFontSmoothing = FALSE;
		SystemParametersInfo( SPI_GETFONTSMOOTHING, 0, &m_bFontSmoothing, 0 );
   }

   ~cFontCache();

   struct cCharMetrics
   {
      short m_nWidth;
   };

   BOOL m_bFontSmoothing;
   SIZE m_szCharCell;
   cCharMetrics m_nWidths[ 256 ];
   LOGFONT m_lf;
   CComPtr< ICanvas > m_pBuffer;

   bool checkBuffer();

BEGIN_COM_MAP(cFontCache)
	COM_INTERFACE_ENTRY(IFontCache)
   COM_INTERFACE_ENTRY(IFontCacheDisp)
   COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IFontCache
public:
	STDMETHOD(HitTest)(BSTR szText, long nPos, /*[out, retval]*/ long *nIndex);
	STDMETHOD(MeasureText)( BSTR szText, /*[out]*/ LPSIZE pszExt );
	STDMETHOD(DrawText)( LPPOINT pt, BSTR szText, long clr, ICanvas *pCanvas );
	STDMETHOD(DrawTextEx)( LPPOINT pt, BSTR szText, long clr1, long clr2, long lFlags, ICanvas *pCanvas );
};

#endif //__FONTCACHE_H_
