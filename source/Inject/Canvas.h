// Canvas.h : Declaration of the cCanvas

#ifndef __CANVAS_H_
#define __CANVAS_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cCanvas
class ATL_NO_VTABLE cCanvas : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public ICanvas
{
public:
	cCanvas()
      : m_hdc( NULL ),
      m_rgn( NULL )
	{
	  m_bf.SourceConstantAlpha = 255;
	  m_bf.BlendOp = AC_SRC_OVER;
	  m_bf.BlendFlags = 0;
	  m_bf.AlphaFormat = 0;
	  sourceAlpha = 255;
	  wRBitCount = 0;
	}

   ~cCanvas();

   CComPtr< IDirectDrawSurface4 > m_pSurface;
   SIZE m_sz;

   typedef std::stack< ClipParams > cClipStack;
   cClipStack m_clipping;

   HDC m_hdc;
   HRGN m_rgn;

   BLENDFUNCTION m_bf;

   WORD wRBitCount, wGBitCount, wBBitCount;
   WORD wRLoBit, wGLoBit, wBLoBit;

   long sourceAlpha;

   void testSurface();
   void processMask(WORD bitMask, WORD *loBit, WORD *bitCount);


BEGIN_COM_MAP(cCanvas)
	COM_INTERFACE_ENTRY(ICanvas)
END_COM_MAP()

// ICanvas
public:
	STDMETHOD(SetTransparentColor)(/*[in]*/ long TransColor);
	STDMETHOD(GetDCLong)(long *DC);
	STDMETHOD(SetClipRect)(LPRECT prcClip, VARIANT_BOOL *pbVisible);
	STDMETHOD(OffsetOrg)(LPPOINT ppt, VARIANT_BOOL *pbVisible);
	STDMETHOD(ToScreen)(/*[in, out]*/ LPPOINT ppt);
	STDMETHOD(ToClient)(/*[in, out]*/ LPPOINT pt);
	STDMETHOD(HitTest)(LPPOINT ppt, /*[out, retval]*/ VARIANT_BOOL *pbHit);
	STDMETHOD(Blt)(LPRECT prcSrc, ICanvas *pSrc, LPPOINT pptDest);
	STDMETHOD(get_Size)(/*[out, retval]*/ LPSIZE pVal);
	STDMETHOD(put_Size)(/*[in]*/ LPSIZE newVal);
	STDMETHOD(get_WasLost)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(Frame)(LPRECT prc, long nRGB);
	STDMETHOD(Fill)(LPRECT prc, long nRGB);
	STDMETHOD(GetClipParams)(/*[out, retval]*/ ClipParams *pParams);
	STDMETHOD(GetSurface)(REFIID iid, /*[out, retval, iid_is(iid)]*/ void **ppvItf);
	STDMETHOD(ReleaseDC)();
	STDMETHOD(GetDC)(/*[out,retval]*/ HDC *pdc);
	STDMETHOD(PopClipRect)();
	STDMETHOD(PushClipRect)(LPRECT prc, /*[out, retval]*/ VARIANT_BOOL *pbVisible);
	STDMETHOD(put_Alpha)(long Alpha);
	STDMETHOD(DownMixRGB)(WORD wRed, WORD wGreen, WORD wBlue, /*[out, retval]*/ WORD *wDMRGB);
	STDMETHOD(StretchBlt)(LPRECT prcSrc, ICanvas *pSrc, LPRECT prcDest);
};

#endif //__CANVAS_H_
