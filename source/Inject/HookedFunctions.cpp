extern void (*pfnOldChatMessage)(char*, unsigned long);
extern void (*pfnOldChatText)(void);

bool DispatchChatMessage ( char *pText, long *pdwColor );
bool DispatchChatText ( char *pText );

void __declspec(naked) OnChatMessage(char* pText, long dwColor)
{
/*	__asm
	{
		//int 3

		push ecx
		push esi
		
		lea esi, dwColor
		push esi
		push pText
		call DispatchChatMessage

		// Pop the two arguments
		add esp, 8

		test al, al
		jz dont_kill_text

		mov esi, pText
		mov BYTE PTR [esi], 0

dont_kill_text:
		pop esi
		pop ecx

		jmp pfnOldChatMessage
	}*/

	__asm
	{
		push    ebp
		mov     ebp, esp
		sub     esp, 20h
		push	esi

		push	ecx

		lea		esi, dwColor
		push	esi
		push	pText
		call	DispatchChatMessage
		
		add		esp, 8

		test	al, al
		jz		dont_kill_text

		mov		DWORD PTR [pText], 0

dont_kill_text:

		pop		ecx

		push	dwColor
		push	pText
		call	pfnOldChatMessage

		pop     esi
		mov     esp, ebp
		pop     ebp
		retn    8
	}
}

void __declspec(naked) OnChatText()
{
	__asm
	{
		push ebp
		mov ebp, esp
		sub esp, 0xC

		call pfnOldChatText

		mov [ebp-4], ecx
		mov [ebp-8], eax

		push eax

		call DispatchChatText

		mov edx, eax
		mov ecx, [ebp-4]
		mov eax, [ebp-8]

		test dl, dl
		jz dont_eat_chat_text

		mov BYTE PTR [eax], 0

dont_eat_chat_text:
		mov esp, ebp
		pop ebp
		ret
	}
}