// WndMsg.h : Declaration of the cWndMsg

#ifndef __WNDMSG_H_
#define __WNDMSG_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cWndMsg
class ATL_NO_VTABLE cWndMsg : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cWndMsg, &CLSID_WndMsg>,
	public IDispatchImpl<IWndMsg, &IID_IWndMsg, &LIBID_DecalInput>
{
public:
	cWndMsg()
	{
	}

   long m_hWnd,
      m_nMsg,
      m_nWParam,
      m_nLParam;
     
   bool m_bEaten;

   void loadMessage( HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam )
   {
      m_hWnd = reinterpret_cast< long >( hWnd );
      m_nMsg = static_cast< long >( nMsg );
      m_nWParam = static_cast< long >( wParam );
      m_nLParam = static_cast< long >( lParam );
      m_bEaten = false;
   }

DECLARE_REGISTRY_RESOURCEID(IDR_WNDMSG)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cWndMsg)
	COM_INTERFACE_ENTRY(IWndMsg)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IWndMsg
public:
	STDMETHOD(get_Eat)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Eat)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_LParam)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_WParam)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Message)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_HWND)(/*[out, retval]*/ long *pVal);
};

#endif //__WNDMSG_H_
