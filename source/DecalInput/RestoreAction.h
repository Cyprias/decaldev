// RestoreAction.h : Declaration of the cRestoreAction

#ifndef __RESTOREACTION_H_
#define __RESTOREACTION_H_

#include "resource.h"       // main symbols
#include <DecalInputImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cRestoreAction
class ATL_NO_VTABLE cRestoreAction : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cRestoreAction, &CLSID_RestoreAction>,
	public IInputActionImpl< cRestoreAction >
{
public:
	cRestoreAction()
	{
	}

   POINT m_pt;

   // Nothing to load really

DECLARE_REGISTRY_RESOURCEID(IDR_RESTOREACTION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cRestoreAction)
	COM_INTERFACE_ENTRY(IInputAction)
END_COM_MAP()

// IRestoreAction
public:
   STDMETHOD(get_Stackable)(VARIANT_BOOL *pVal)
   {
      *pVal = VARIANT_TRUE;
      return S_OK;
   }

   STDMETHOD(Push)()
   {
      ::GetCursorPos( &m_pt );
      return S_OK;
   }

   STDMETHOD(Pop)()
   {
      m_pSite->MoveMouse( m_pt.x, m_pt.y );
      return S_OK;
   }
};

#endif //__RESTOREACTION_H_
