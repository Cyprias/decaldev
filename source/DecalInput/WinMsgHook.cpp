// WinMsgHook.cpp : Implementation of cWinMsgHook
#include "stdafx.h"
#include "DecalInput.h"
#include "WinMsgHook.h"

#include "InputService.h"

/////////////////////////////////////////////////////////////////////////////
// cWinMsgHook


STDMETHODIMP cWinMsgHook::get_Tag(VARIANT *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   return ::VariantCopy( pVal, &m_tag );
}

STDMETHODIMP cWinMsgHook::put_Tag(VARIANT newVal)
{
   m_tag = newVal;

	return S_OK;
}

STDMETHODIMP cWinMsgHook::get_Enabled(VARIANT_BOOL *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = ( m_bEnabled ) ? VARIANT_TRUE : VARIANT_FALSE;

	return S_OK;
}

STDMETHODIMP cWinMsgHook::put_Enabled(VARIANT_BOOL newVal)
{
   if( !!newVal == m_bEnabled )
      return S_FALSE;

   if( cInputService::g_p == NULL )
   {
      // Input Service must be started to enable or disable a hotkey
      _ASSERT( FALSE );
      return E_FAIL;
   }

   m_bEnabled = !!newVal;

   if( m_bEnabled )
   {
#ifdef _DEBUG
      for( cInputService::cWinMsgHookList::iterator i = cInputService::g_p->m_msghooks.begin(); i != cInputService::g_p->m_msghooks.end(); ++ i )
         _ASSERTE( *i != this );
#endif

      cInputService::g_p->m_msghooks.push_back( this );
   }
   else
   {
      for( cInputService::cWinMsgHookList::iterator i = cInputService::g_p->m_msghooks.begin(); i != cInputService::g_p->m_msghooks.end(); ++ i )
      {
         if( *i == this )
         {
            cInputService::g_p->m_msghooks.erase( i );
            break;
         }
      }

      _ASSERTE( i != cInputService::g_p->m_msghooks.end() );
   }

	return S_OK;
}
