
DecalInputps.dll: dlldata.obj DecalInput_p.obj DecalInput_i.obj
	link /dll /out:DecalInputps.dll /def:DecalInputps.def /entry:DllMain dlldata.obj DecalInput_p.obj DecalInput_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DecalInputps.dll
	@del DecalInputps.lib
	@del DecalInputps.exp
	@del dlldata.obj
	@del DecalInput_p.obj
	@del DecalInput_i.obj
